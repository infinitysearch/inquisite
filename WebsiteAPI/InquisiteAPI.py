from flask import Flask, Blueprint, render_template, request, make_response, redirect, url_for, jsonify
import TextAnalyzer
import nltk

inquisiteAPI = Blueprint('inquisiteAPI', __name__)


def get_analysis_text(text, link=''):
    analyzed_text = TextAnalyzer.analyze_raw_text(text)

    summary = analyzed_text['summary']
    entities = analyzed_text['entities']
    keywords = analyzed_text['keywords']
    words = analyzed_text['words']
    verbs = analyzed_text['verbs']
    nouns = analyzed_text['nouns']
    links = analyzed_text['links']

    if link != '':
        links = TextAnalyzer.get_links_from_page(link)
        links = nltk.FreqDist(links).most_common()

    # print(summary)
    # print(keywords)
    # print(entities)
    # print(links)
    # print(words)
    # print(verbs)
    # print(nouns)
    # print(links)

    return render_template('inquisite.html', text=text, summary=summary, keywords=keywords, entities=entities,
                           links=links, words=words, verbs=verbs, nouns=nouns, link=link, analyze=True)


def get_analysis_link(link):
    text = TextAnalyzer.get_raw_text(link)
    return get_analysis_text(text, link)


@inquisiteAPI.route('/', methods=['GET', 'POST'])
def render_inquisite():
    if request.method == 'GET':
        return render_template('inquisite.html', analyze=False)

    if request.method == 'POST':
        post_data = dict(request.form)

        if 'link_radio' in post_data:
            if 'link' in post_data:
                if post_data['link'] != '':
                    return get_analysis_link(post_data['link'])

        if 'text_radio' in post_data:
            if 'text' in post_data:
                return get_analysis_text(post_data['text'])

    return redirect('/')


def get_analysis_text_json(text, link=''):
    analyzed_text = TextAnalyzer.analyze_raw_text(text)
    if link != '':
        links = TextAnalyzer.get_links_from_page(link)
        links = nltk.FreqDist(links).most_common()
        analyzed_text['links'] = links

    return jsonify(analyzed_text)


def get_analysis_link_json(link):
    text = TextAnalyzer.get_raw_text(link)
    return get_analysis_text_json(text, link)


@inquisiteAPI.route('/get_text_analysis', methods=['GET', 'POST'])
def get_text_analysis_json():
    if request.method == 'POST':
        post_data = dict(request.form)

        if 'link' in post_data:
            if post_data['link'] != '':
                return get_analysis_link_json(post_data['link'])

        if 'text' in post_data:
            return get_analysis_text_json(post_data['text'])

        return jsonify({'error': 'no link or text provided'})

    try:
        if 'link' in request.args:
            if request.args.get('link') != '':
                return get_analysis_link_json(request.args.get('link'))

        if 'text' in request.args:
            if request.args.get('text') != '':
                return get_analysis_text_json(request.args.get('text'))

        return jsonify({'error': 'no link or text provided'})

    except Exception:
        return jsonify({'error': 'There was an error while processing your request'})
