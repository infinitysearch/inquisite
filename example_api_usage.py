import requests

# This example is in Python but this will work over any HTTP client

# Run this while the wsgi.py for it to work correctly

# Get request Options

get_option_text = requests.get('http://localhost:5000/get_text_analysis?text=put any text here').json()
get_option_link = requests.get('http://localhost:5000/get_text_analysis?link=https://infinitysearch.co/about').json()

print(get_option_link)
print(get_option_text)


# Post request Options
post_option_text = requests.post('http://localhost:5000/get_text_analysis', {'text' : 'Insert any text here.'}).json()
post_option_link = requests.post('http://localhost:5000/get_text_analysis', {'link' : 'https://infinitysearch.co/about'}).json()

print(post_option_text)
print(post_option_link)

