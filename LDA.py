import nltk
import gensim
from gensim import corpora
import random
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
import string

# This algorithm was taken from https://radimrehurek.com/gensim/auto_examples/tutorials/run_lda.html#sphx-glr-auto-examples-tutorials-run-lda-py
# This is not currently integrated into Inquisite because the calculations take too long but it could be useful in many scenarios

def clean(doc):
    stop = set(stopwords.words('english'))
    exclude = set(string.punctuation)
    lemma = WordNetLemmatizer()

    stop_free = " ".join([i for i in doc.lower().split() if i not in stop])
    punc_free = ''.join(ch for ch in stop_free if ch not in exclude)
    normalized = " ".join(lemma.lemmatize(word) for word in punc_free.split())
    return normalized


def get_lda(text):
    doc_complete = nltk.sent_tokenize(text)
    doc_clean = [clean(doc).split() for doc in doc_complete]
    dictionary = corpora.Dictionary(doc_clean)
    doc_term_matrix = [dictionary.doc2bow(doc) for doc in doc_clean]
    Lda = gensim.models.ldamodel.LdaModel
    num_topics = random.randint(0, 20)
    ldamodel = Lda(doc_term_matrix, num_topics=num_topics, id2word=dictionary, passes=50)
    topics = ldamodel.print_topics()
    return topics

if __name__ == '__main__':
    x = 0
