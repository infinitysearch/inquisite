from flask import Flask, render_template, request, flash, session, redirect, url_for, send_from_directory
import os

from WebsiteAPI.InquisiteAPI import inquisiteAPI

app = Flask(__name__)

app.register_blueprint(inquisiteAPI)

@app.after_request
def add_header(response):
    # This is needed for API access to work correctly
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'GET, POST'

    return response


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico',
                               mimetype='image/vnd.microsoft.icon')


if __name__ == '__main__':
    app.run()
