# Inquisite
Summarize and extract information from texts and links.

![dashboard](static/images/Dashboard.png)

## How it works 
Inquisite works by combining several natural language processing features from 
nltk, spacy, and gensim into one general analysis of the text.

## Installation 

This project requires any Python3 and pip3 version and the packages found in the [requirement.txt](requirements.txt) 
file to run it. Once you have Python3 installed, you can run
```shell script
pip3 install -r requirements.txt 
```

Then, If you have never used the spacy and nltk libraries, you will also need to run these commands as well:
```shell script
python3 -m spacy download en_core_web_sm
```
and in python run:
```python
import nltk
nltk.download('popular')
```
You only need to do that part once. 

### Running Locally 
Once you have installed everything, make sure that you are
in this repository and run

```shell script
python3 wsgi.py 
```
You should then be able to find Inquisite at https://localhost:5000 or http://127.0.0.1:5000. 

#### Deployment 
If you want to run Inquisite in a production environment, you can deploy this
application like any other Flask one. Flask has deployment options and 
documentation at https://flask.palletsprojects.com/en/1.1.x/deploying. 

Here is an example of how to run the program behind and NGINX server at at https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-18-04. 

## Usage 
### Browser
If you are running locally, just go to http://127.0.0.1:5000 or http://localhost:5000 once wsgi.py is running to view the interface on your browser.
### API 
There is also a JSON API for accessing the results. To use it, you can access it by get and requests. Go to 
[example_api_usage.py](example_api_usage.py) for examples of how to use it. 

