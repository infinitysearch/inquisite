import requests
from urllib.parse import urlparse
from bs4 import BeautifulSoup
from random import choice
import spacy
import html2text
import nltk
import re

from nltk.stem import WordNetLemmatizer
from nltk import FreqDist

from gensim.summarization import summarize
from gensim.summarization import keywords

desktop_agents = [
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'
]


def random_headers():
    return {'User-Agent': choice(desktop_agents),
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'}


def get_links_from_page(url):
    parsed_main_url = urlparse(url)
    links = []
    page = requests.get(url, headers=random_headers()).text
    soup = BeautifulSoup(page, 'html.parser')
    for link in soup.find_all('a'):
        link_name = str(link.get('href'))
        parsed_link = urlparse(link_name)

        if parsed_link[1] != '':
            links.append(link_name)

        elif parsed_link[2] != 'None':
            if parsed_link[2].startswith('/'):
                links.append(parsed_main_url[0] + '://' + parsed_main_url[1] + parsed_link[2])
            else:
                if parsed_main_url[2].endswith('/'):
                    links.append(parsed_main_url[0] + '://' + parsed_main_url[1] + parsed_main_url[2] + parsed_link[2])
                else:
                    links.append(
                        parsed_main_url[0] + '://' + parsed_main_url[1] + parsed_main_url[2] + '/' + parsed_link[2])

    return links


def get_links_from_bs4_text(soup, url):
    parsed_main_url = urlparse(url)
    links = []
    for link in soup.find_all('a'):
        link_name = str(link.get('href'))
        parsed_link = urlparse(link_name)
        if parsed_link[1] != '':
            links.append(link_name)

        elif parsed_link[2] != 'None':
            if parsed_link[2].startswith('/'):
                links.append(parsed_main_url[0] + '://' + parsed_main_url[1] + parsed_link[2])
            else:
                if parsed_main_url[2].endswith('/'):
                    links.append(parsed_main_url[0] + '://' + parsed_main_url[1] + parsed_main_url[2] + parsed_link[2])
                else:
                    links.append(
                        parsed_main_url[0] + '://' + parsed_main_url[1] + parsed_main_url[2] + '/' + parsed_link[2])

    return links


def get_raw_text(url):
    h = html2text.HTML2Text()
    h.ignore_links = True
    h.ignore_images = True
    h.ignore_emphasis = True
    h.ignore_tables = True
    return h.handle(requests.get(url).text)


def get_entities(text):
    nlp = spacy.load("en_core_web_sm")
    doc = nlp(text)
    entities = []

    for entity in doc.ents:
        entities.append([entity.text, entity.label_])

    return entities


def get_nouns(text):
    nlp = spacy.load("en_core_web_sm")
    doc = nlp(text)
    return [chunk.text for chunk in doc.noun_chunks]


def get_verbs(text):
    nlp = spacy.load("en_core_web_sm")
    doc = nlp(text)
    return [token.lemma_ for token in doc if token.pos_ == "VERB"]


def get_text_without_stopwords(text):
    nlp = spacy.load("en_core_web_sm")
    my_doc = nlp(text)
    # Create list of word tokens
    token_list = []
    for token in my_doc:
        token_list.append(token.text)

    # Create list of word tokens after removing stopwords
    filtered_sentence = ''

    extra_stops = ['.', ',', '-', "'", '`', '"', '<', '>', '?', '...', '~', '@', '#', '$', '%', '^', '&', '*', '(', ')'
        , '+', '=', '|', '{', '}', '[', ']', '/', '::', '--', '_', ':', '- -']

    for word in token_list:

        if word in extra_stops:
            continue

        lexeme = nlp.vocab[word]
        if lexeme.is_stop == False:
            # filtered_sentence.append(word)
            filtered_sentence += word + ' '
    return filtered_sentence


def lemmatize_text(tokenization):
    wordnet_lemmatizer = WordNetLemmatizer()
    # tokenization = nltk.word_tokenize(text)
    words = []
    for w in tokenization:
        words.append(wordnet_lemmatizer.lemmatize(w))

    return words


def lemmatize_spacy(text):
    nlp = spacy.load("en_core_web_sm")
    doc = nlp(text)
    lemmas = []
    for token in doc:
        lemmas.append(token.lemma_)

    return lemmas


def analyze_raw_text(text):
    all_info = {}
    text = text.replace('\n', ' ')
    summary = ''
    try:
        # summary = summarize(text, word_count=100)
        summary = summarize(text)

    except Exception:
        x = 0

    all_keywords = keywords(text)
    entities = get_entities(text)

    sole_entities = []
    for entity in entities:
        sole_entities.append(entity[0])

    entity_dist = FreqDist(sole_entities).most_common()

    formatted_entities = []
    for entity in entity_dist:
        # entity = entity.extend('cool')
        for e in entities:
            if e[0] == entity[0]:
                formatted_entities.append([entity[0], entity[1], e[1]])
                break

    links = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+] |[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', text)

    # print(links)
    all_keywords = all_keywords.replace('\n', ' ')
    all_keywords = lemmatize_spacy(all_keywords)
    all_keywords = FreqDist(all_keywords).keys()

    all_keywords = list(all_keywords)

    # print(all_keywords)

    # pos_tags = nltk.pos_tag(nltk.word_tokenize(text))

    nouns = get_nouns(text)
    verbs = get_verbs(text)

    # print('Summary:', summary)
    # print('Entities:', entities)
    # print('POS Tags:', pos_tags)
    # print('Nouns:', nouns)
    # print('Verbs:', verbs)
    # print('keywords:', all_keywords)

    # keyword_dist = FreqDist(all_keywords)
    # word_dist = FreqDist(nltk.word_tokenize(text))

    verb_dist = FreqDist(verbs)
    noun_dist = FreqDist(nouns)

    # print('Keyword FreqDist:', keyword_dist.most_common())
    # print('All Words FreqDist:', word_dist.most_common())

    text_without_stopwords = get_text_without_stopwords(text)
    word_dist_without_stop = FreqDist(nltk.word_tokenize(text_without_stopwords))

    # print('All Words Without Stop Words FreqDist:', word_dist_without_stop.most_common())

    return {'text': text, 'summary': summary, 'keywords': all_keywords,
            'words': word_dist_without_stop.most_common(), 'verbs': verb_dist.most_common(),
            'nouns': noun_dist.most_common(), 'entities': formatted_entities, 'links': links}



if __name__ == '__main__':
    data = ('''
    The goal of this repository is to build a comprehensive set of tools and examples that leverage recent advances in NLP algorithms, neural architectures, and distributed machine learning systems. The content is based on our past and potential future engagements with customers as well as collaboration with partners, researchers, and the open source community.
    We hope that the tools can significantly reduce the “time to market” by simplifying the experience from defining the business problem to development of solution by orders of magnitude. In addition, the example notebooks would serve as guidelines and showcase best practices and usage of the tools in a wide variety of languages.
    In an era of transfer learning, transformers, and deep architectures, we believe that pretrained models provide a unified solution to many real-world problems and allow handling different tasks and languages easily. We will, therefore, prioritize such models, as they achieve state-of-the-art results on several NLP benchmarks like GLUE and SQuAD leaderboards. The models can be used in a number of applications ranging from simple text classification to sophisticated intelligent chat bots.
    Note that for certain kind of NLP problems, you may not need to build your own models. Instead, pre-built or easily customizable solutions exist which do not require any custom coding or machine learning expertise. We strongly recommend evaluating if these can sufficiently solve your problem. If these solutions are not applicable, or the accuracy of these solutions is not sufficient, then resorting to more complex and time-consuming custom approaches may be necessary. The following cognitive services offer simple solutions to address common NLP tasks: 
    ''')

    print(analyze_raw_text(data))
